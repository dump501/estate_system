/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { message: 'The server is up and running...' }
})

Route.group(()=>{
  Route.post("register", "AuthController.register")
  Route.post("login", "AuthController.login")
})
.prefix("api")
.namespace('App/Controllers/Http/Api')


Route.group(()=>{
  // user
  Route.post("user/:id/approve", "UserController.approve")

  // city
  Route.get("city", "CitiesController.index")
  Route.post("city", "CitiesController.store")
  Route.patch("city/:id", "CitiesController.update")
  Route.delete("city/:id", "CitiesController.delete")

  // region
  Route.get("region", "RegionsController.index")
  Route.post("region", "RegionsController.store")
  Route.patch("region/:id", "RegionsController.update")
  Route.delete("region/:id", "RegionsController.delete")

  // comment
  Route.get("comment", "CommentsController.index")
  Route.post("comment", "CommentsController.store")
  Route.patch("comment/:id", "CommentsController.update")
  Route.delete("comment/:id", "CommentsController.delete")

  // house
  Route.get("house", "HousesController.index")
  Route.post("house", "HousesController.store")
  Route.patch("house/:id", "HousesController.update")
  Route.get("house/:id", "HousesController.show")
  Route.delete("house/:id", "HousesController.delete")
  Route.post("house/search", "HousesController.search")

  // house type
  Route.get("house-type", "HouseTypesController.index")
  Route.post("house-type", "HouseTypesController.store")
  Route.delete("house-type/:id", "HouseTypesController.delete")

  // user
  Route.get("user", "UsersController.index")
  Route.post("user", "UsersController.store")
  Route.patch("user/:id", "UsersController.update")
  Route.delete("user/:id", "UsersController.delete")
})
.prefix("api/admin")
// .middleware("auth:api")
.namespace('App/Controllers/Http/Api/Admin')
