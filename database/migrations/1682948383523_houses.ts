import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'houses'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('length').notNullable()
      table.integer('width').notNullable()
      table.integer('mounth_price').notNullable()
      table.integer('year_price').notNullable()
      table.string('image', 255).notNullable()
      table.string('image1', 255).nullable()
      table.string('image2', 255).nullable()
      table.string('image3', 255).nullable()
      table.string('image4', 255).nullable()
      table.integer('region_id').notNullable()
      table.integer('city_id').notNullable()
      table.integer('user_id').notNullable()
      table.integer('house_type_id').notNullable()
      table.boolean('is_valid').defaultTo(false)
      table.string('quater').notNullable()
      table.string('location').notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
