import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'houses'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.string("name")
      table.string("description")
    })
  }
}
