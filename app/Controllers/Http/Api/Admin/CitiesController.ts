import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import City from 'App/Models/City'
import CityValidator from 'App/Validators/CityValidator'

export default class CitiesController {
    async index({response}: HttpContextContract){
        try {
            
            let cities = await City.query().preload("region")
            response.status(200)
            return {
                data: cities
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async store({request, response}: HttpContextContract){
        try {
            let data = await request.validate(CityValidator)
            let city = new City()
            city.fill({
                name: data.name,
                region_id: data.region_id
            })
            .save()
            response.status(200)
            return {
                data: city
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async update({request, params, response}: HttpContextContract){
        try {
            let data = await request.validate(CityValidator)
            if(data){
                
                await City
                .query()
                .where("id", params.id)
                .update({
                    name: data.name,
                    region_id: data.region_id
                })
                response.status(200);

                return{
                    data
                }
            }
            response.status(404);

            return{
                error: "Id not found"
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async delete({params, response}: HttpContextContract){
        try {
            let city = await City.findOrFail(params.id);
            if (city) {
                city.delete()
                response.status(200);

                return {
                    data: city
                }
            }
            response.status(404);

            return{
                error: "id not found"
            }
            
        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }
}
