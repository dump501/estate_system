import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Comment from 'App/Models/Comment'
import CommentValidator from 'App/Validators/CommentValidator'

export default class CommentsController {

    async store({request, response}: HttpContextContract){
        try {
            let data = await request.validate(CommentValidator)
            let comment = new Comment()
            comment.fill({
                comment: data.comment,
                user_id: data.user_id,
                house_id: data.house_id,
            })
            .save()
            response.status(200)
            return {
                data: comment
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async delete({params, response}: HttpContextContract){
        try {
            let comment = await Comment.findOrFail(params.id);
            if (comment) {
                comment.delete()
                response.status(200);

                return {
                    data: comment
                }
            }
            response.status(404);

            return{
                error: "id not found"
            }
            
        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }
}
