import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import House from 'App/Models/House';
import Env from '@ioc:Adonis/Core/Env'
import HouseValidator from 'App/Validators/HouseValidator';

export default class HousesController {
    async index({response}: HttpContextContract){
        try {

            let houses = await House.query().preload("city").preload("region").preload("houseType").preload("user")
            response.status(200)
            return {
                data: houses
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }
    async show({params, response}: HttpContextContract){
        try {

            let house = await House.query().where("id", params.id).preload("user").preload("city").preload("region").preload("houseType").firstOrFail()
            response.status(200)
            return {
                data: house
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async store({request, response}: HttpContextContract){
        try {
            let data = await request.validate(HouseValidator)
            let house = new House()
            let timestamp = Date.now()
            let root = `/uploads/images/`
            let image = `file_${timestamp}.${request.file("image")?.extname}`
            console.log(image);
            await request.file("image")?.move("./public/uploads/images", {
                name: image
            })
            let image1 = `file1_${timestamp}.${request.file("image1")?.extname}`
            await request.file("image1")?.move("./public/uploads/images", {
                name: image1
            })
            let image2 = `file2_${timestamp}.${request.file("image2")?.extname}`
            await request.file("image2")?.move("./public/uploads/images", {
                name: image2
            })
            let image3 = `file3_${timestamp}.${request.file("image3")?.extname}`
            await request.file("image3")?.move("./public/uploads/images", {
                name: image3
            })
            let image4 = `file4_${timestamp}.${request.file("image4")?.extname}`
            await request.file("image4")?.move("./public/uploads/images", {
                name: image4
            })
            console.log(image,image1,image2,image3,image4);
            house.fill({
                mounthPrice: data.mounthPrice,
                yearPrice: data.yearPrice,
                name: data.name,
                description: data.description,
                length: data.length,
                width: data.width,
                image: root + image,
                image1: root + image1,
                image2: root + image2,
                image3: root + image3,
                image4: root + image4,
                user_id: data.user_id,
                region_id: data.region_id,
                city_id: data.city_id,
                house_type_id: data.house_type_id,
                quater: data.quater,
                location: data.location,
            })
            .save()
            console.log(house);
            response.status(200)
            return {
                data: house
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async update({request, params, response}: HttpContextContract){
        try {
            let data = await request.validate(HouseValidator)
            if(data){

                await House
                .query()
                .where("id", params.id)
                .update({
                    mounthPrice: data.mounthPrice,
                    yearPrice: data.yearPrice,
                    length: data.length,
                    width: data.width,
                    user_id: data.user_id,
                    region_id: data.region_id,
                    city_id: data.city_id
                })
                response.status(200);

                return{
                    data
                }
            }
            response.status(404);

            return{
                error: "Id not found"
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async delete({params, response}: HttpContextContract){
        try {
            let house = await House.findOrFail(params.id);
            if (house) {
                house.delete()
                response.status(200);

                return {
                    data: house
                }
            }
            response.status(404);

            return{
                error: "id not found"
            }

        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }

    async search({ request, response }: HttpContextContract) {
        try {
            console.log(request.input("city_id"));

            let cities = await House.query().where("city_id", request.input("city_id")).andWhere("region_id",  request.input("region_id")).andWhere("house_type_id",  request.input("house_type_id")).preload("city").preload("region").preload("houseType").preload("user")
            return{
                data: {
                    cities
                }
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }
}
