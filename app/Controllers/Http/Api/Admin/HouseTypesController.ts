import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import HouseType from 'App/Models/HouseType';
import HomeTypeValidator from 'App/Validators/HomeTypeValidator';

export default class HouseTypesController {

    async index({response}: HttpContextContract){
        try {
            
            let houseType = await HouseType.all()
            response.status(200)
            return {
                data: houseType
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async store({request, response}: HttpContextContract){
        try {
            let data = await request.validate(HomeTypeValidator)
            let houseType = new HouseType()
            houseType.fill({
                name: data.name
            })
            .save()
            response.status(200)
            return {
                data: houseType
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    // async update({request, params, response}: HttpContextContract){
    //     try {
    //         let data = await request.validate(RegionValidator)
    //         if(data){
                
    //             await Region
    //             .query()
    //             .where("id", params.id)
    //             .update({
    //                 name: data.name
    //             })
    //             response.status(200);

    //             return{
    //                 data
    //             }
    //         }
    //         response.status(404);

    //         return{
    //             error: "Id not found"
    //         }
    //     } catch (error) {
    //         response.status(500);
    //         return{
    //             error: error.messages,
    //         }
    //     }
    // }

    async delete({params, response}: HttpContextContract){
        try {
            let houseType = await HouseType.findOrFail(params.id);
            if (houseType) {
                houseType.delete()
                response.status(200);

                return {
                    data: houseType
                }
            }
            response.status(404);

            return{
                error: "id not found"
            }
            
        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }
}
