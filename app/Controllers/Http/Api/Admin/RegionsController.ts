import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Region from 'App/Models/Region';
import RegionValidator from 'App/Validators/RegionValidator';

export default class RegionsController {
    async index({response}: HttpContextContract){
        try {
            
            let regions = await Region.all()
            response.status(200)
            return {
                data: regions
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async store({request, response}: HttpContextContract){
        try {
            let data = await request.validate(RegionValidator)
            let region = new Region()
            region.fill({
                name: data.name
            })
            .save()
            response.status(200)
            return {
                data: region
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async update({request, params, response}: HttpContextContract){
        try {
            let data = await request.validate(RegionValidator)
            if(data){
                
                await Region
                .query()
                .where("id", params.id)
                .update({
                    name: data.name
                })
                response.status(200);

                return{
                    data
                }
            }
            response.status(404);

            return{
                error: "Id not found"
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async delete({params, response}: HttpContextContract){
        try {
            let region = await Region.findOrFail(params.id);
            if (region) {
                region.delete()
                response.status(200);

                return {
                    data: region
                }
            }
            response.status(404);

            return{
                error: "id not found"
            }
            
        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }
}
