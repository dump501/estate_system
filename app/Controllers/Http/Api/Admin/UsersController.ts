import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User';

export default class UsersController {
    async index({response}: HttpContextContract){
        try {

            let users = await User.query().preload("role")
            response.status(200)
            return {
                data: users
            }
        } catch (error) {
            response.status(500);
            return{
                error: error.messages,
            }
        }
    }

    async approve({params, response}: HttpContextContract){
        try {
            let user = await User.query().where("id", params.id).update({is_valid: true})
            if(user){
                response.status(200)
                return{
                    data: user
                }
            }
            response.status(400)
            return {
                error: "The user do not exist"
            }

        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }

    async desapprove({params, response}: HttpContextContract){
        try {
            let user = await User.query().where("id", params.id).update({is_valid: false})
            if(user){
                response.status(200)
                return{
                    data: user
                }
            }
            response.status(400)
            return {
                error: "The user do not exist"
            }

        } catch (error) {
            response.status(500);
            return{
                error: error.messages
            }
        }
    }
}
