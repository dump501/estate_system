import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, HasMany, belongsTo, column, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Region from './Region'
import City from './City'
import Comment from './Comment'
import User from './User'
import HouseType from './HouseType'

export default class House extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public length: number

  @column()
  public width: number

  @column()
  public image: string

  @column()
  public image1: string | null

  @column()
  public image2: string | null

  @column()
  public image3: string | null

  @column()
  public image4: string | null

  @column()
  public region_id: number

  @column()
  public user_id: number

  @column()
  public city_id: number

  @column()
  public house_type_id: number

  @column()
  public yearPrice: number

  @column()
  public is_valid: boolean

  @column()
  public location: string

  @column()
  public quater: string

  @column()
  public name: string

  @column()
  public description: string

  @column()
  public mounthPrice: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  // relationships

  @belongsTo(() => Region, {
    foreignKey: "region_id"
  })
  public region: BelongsTo<typeof Region>

  @belongsTo(() => City, {
    foreignKey: "city_id"
  })
  public city: BelongsTo<typeof City>

  @belongsTo(() => User, {
    foreignKey: "user_id"
  })
  public user: BelongsTo<typeof User>

  @hasMany(() => Comment)
  public comments: HasMany<typeof Comment>

  @belongsTo(() => HouseType, {
    foreignKey: "house_type_id"
  })
  public houseType: BelongsTo<typeof HouseType>
}
