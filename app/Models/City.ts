import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, HasMany, belongsTo, column, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Region from './Region'
import House from './House'

export default class City extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  
  @column()
  public name: string

  @column()
  public region_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  // relationship

  @belongsTo(() => Region, {
    foreignKey: "region_id"
  })
  public region: BelongsTo<typeof Region>

  @hasMany(() => House)
  public houses: HasMany<typeof House>
}
