import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, HasMany, beforeSave, belongsTo, column, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Comment from './Comment'
import House from './House'
import Hash from '@ioc:Adonis/Core/Hash'
import Role from './Role'

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column()
  public description: string

  @column()
  public phone: string

  @column({ serializeAs: null })
  @column()
  public password: string

  @column()
  public civility: string

  @column()
  public avatar: string | null

  @column()
  public is_valid: boolean

  @column()
  public role_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  // relationships

  @hasMany(() => Comment)
  public comments: HasMany<typeof Comment>

  @hasMany(() => House)
  public houses: HasMany<typeof House>

  @belongsTo(() => Role, {
    foreignKey: "role_id"
  })
  public role: BelongsTo<typeof Role>
}
