import { AssetsConfig } from "@ioc:Adonis/Core/Static";

const staticConfig: AssetsConfig = {
    enabled: true
}

export default staticConfig